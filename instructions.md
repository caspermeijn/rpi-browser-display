<!--
SPDX-License-Identifier: CC-BY-SA-4.0
Copyright (C) 2020 Casper Meijn <casper@meijn.net>

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
  send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

Instructions
============

Needed hardware
---------------
- Raspberry Pi 3B+
- USB power adaptor
- SD card (minimal 8GB, A2 class preferred)
- HDMI monitor + cable
- USB keyboard and mouse

SD card setup
-------------
The SD card needs to be configured with NOOBS, which eases the installation. This could be a pre-installed SD card from a vendor or you can install it on an empty SD card.

To install it yourself:
- Go to https://www.raspberrypi.org/downloads/noobs/ and download the "Network install only"
- Format the SD card using fat32
- Extract the NOOBS zip to the SD card

Hardware setup
--------------
- Insert the SD card into the Raspberry Pi
- Connect the Raspberry Pi to the HDMI monitor
- Connect the USB keyboard and mouse
- Optionally, connect ethernet

Install OS
----------
- Plug in the USB power adaptor to power on the Pi
- The display show the system starting up
- After the system is loaded, choose "Raspberry Pi OS (32-bit)" and click install
- The operating system will now be downloaded (this may take a while)
- Accept the message "OS installed successfully"
- The device will restart and after booting a welcome dialog will appear. Click next
- Choose Country, select "Use US keyboard" and click next
- Optionally change the password and click next
- The black border option is monitor related, so choose the option that fits the monitor that is finally deployed and click next
- Connect to a wireless network or click skip
- Click next and wait for the updates (this may take a while)
- Click later to restart later

Firefox setup
-------------
- Open a terminal
- Run `sudo apt install -y firefox-esr`
- After installation, start "Firefox ESR" from the start menu under the "Internet" category
- Go to the Solar portal website and login
- Save the password in Firefox when asked
- Once on the dashboard, set this page as homepage by dragging and dropping the browser tab to the Home icon.
- Go to 'about:config'
- Click "I accept the risk!"
- Search for "sessionstore"
- Double click "browser.sessionstore.resume_from_crash" so that value is set to "false"
- Go to 'https://addons.mozilla.org/nl/firefox/addon/autofullscreen/' and click "Add to Firefox and later click "Add"
- The browser will be full-screened. Press F11 to exit fullscreen 
- Go to 'https://addons.mozilla.org/nl/firefox/addon/auto-clicker-autofill-beta/' and click "Add to Firefox and later click "Add"
- Close firefox and start it again
- You will end up on the login page, with the username and password filled in
- Press F11 to exit fullscreen 
- Right click the login button and choose "Auto Clicker - AutoFill"
- A new tab is opened, close that
- Refresh the login page. 
- After a long wait it will automatically login and you end up on the dashboard.
- Close firefox and start it again

Nightly reboot
--------------
- Open a terminal
- Run `sudo crontab -e`
- Press enter to accept the default editor
- A text editor is opened, add a new line to the bottom of the file with the following content `0 0 * * * /sbin/reboot
- Press Ctrl+X to exit, press y to save and press enter to accept the filename
- Close terminal

Network Monitor
------------------
- Open a terminal
- Run `wget https://gitlab.com/caspermeijn/rpi-browser-display/-/raw/master/network_monitor.sh`
- Run `chmod +x network_monitor.sh`
- Run `sudo nano /etc/xdg/lxsession/LXDE-pi/autostart`
- A text editor is opened, add with the following content to the bottom of the file:
```
@xset s noblank
@xset s off
@xset -dpms 
@/home/pi/network_monitor.sh
```
- Press Ctrl+X to exit, press y to save and press enter to accept the filename
- Run `reboot`
- After the reboot the solar panel dashboard appear automatically