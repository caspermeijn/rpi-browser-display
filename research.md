<!--
SPDX-License-Identifier: CC-BY-SA-4.0
Copyright (C) 2020 Casper Meijn <casper@meijn.net>

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
  send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

Research
========

Hardware
--------

The Raspberry Pi is an obvious choice, it is small and has a lot of software available. It has an ethernet network connection and the option for WiFi.

I have a Raspberry Pi 3 Model B+ laying around, so that could be a great development board.

In a previous project a used a A2 class sd card, which was blazingly fast.

Software
--------

I think raspbian OS is a great starting point for a graphical project. 

Then we need a browser with plugin support. I like Firefox as it is fully open source.

The website has login form. I think we can use a auto-login plugin to navigate this automatically.

Firefox auto start
------------------

sudo apt install firefox-esr

[This webpage](https://raspberry-projects.com/pi/pi-operating-systems/raspbian/gui/auto-run-browser-on-startup) has some ideas about auto-starting a browser. The correct path is /etc/xdg/lxsession/LXDE-pi/autostart.

[This webpage](https://raspberrypi.stackexchange.com/questions/6981/auto-refresh-for-midori) has some ideas about refreshing the page.

Error messages will end up in ~/.xsession-errors

[This webpage](https://stackoverflow.com/questions/19516212/how-to-gently-kill-firefox-process-on-linux-os-x) talks about disabling restore from crash.



Auto login
----------

https://addons.mozilla.org/en-US/firefox/addon/vt-autologin/?src=search

This plugin performs a submit on the form. However the portal website needs a click on a button.



https://addons.mozilla.org/nl/firefox/addon/auto-clicker-autofill-beta/?src=search

This plugin just clicks the login button. You can configure it by right clicking the login button and selecting the "Auto Clicker" option. The username/password can be saved to Firefox itself. The URL can be set as homepage.

Fullscreen
----------

https://addons.mozilla.org/nl/firefox/addon/autofullscreen/?src=search

This plugin will fullscreen the browser during startup.


Network change
--------------

To reset the browser after a network change you could periodically ping a known good address. If successful you start the browser is unsuccessful you stop the browser.

This could be implemented as a bash script, which periodically ping. If ping is successful start firefox, else kill firefox.
