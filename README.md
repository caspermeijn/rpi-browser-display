<!--
SPDX-License-Identifier: CC-BY-SA-4.0
Copyright (C) 2020 Casper Meijn <casper@meijn.net>

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
  send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

Raspberry PI Browser Display
============================

Instructions for building a Raspberry PI which displays a simple browser page

Problem statement
-----------------
A customer wants to show the results of the installation of their solar panel prominently in their store. The solar power converter manafacturer has a website with a nice dashboard showing the performance of the solar panels. This project should result in a device that shows that website on a TV screen.

Research
--------
See [Research](research.md).

Instructions
------------
See [Instructions](instructions.md)
