#!/bin/bash
set -eu

# SPDX-License-Identifier: CC-BY-SA-4.0
# Copyright (C) 2020 Casper Meijn <casper@meijn.net>
# 
# This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. 
# To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or 
#   send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

ensure_state(){
  if [ "$1" == "start" ]; then
    if ! pgrep firefox; then
      firefox&
    fi
  else
    pkill firefox || true
  fi
}

while true
do
  if ping 8.8.8.8 -c 1 -W 10
  then
    ensure_state start
    sleep 60
  else
    ensure_state stop
    sleep 1
  fi
done
